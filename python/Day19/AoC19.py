import re

text = '''0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb'''

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

messages = []
idx = 0
rule_texts = {}
l = lines[idx]
while l != "":
    (i, text) = tuple(l.split(":"))
    rule_texts[int(i)] = text.strip()
    idx += 1
    l = lines[idx]
idx+=1
while idx < len(lines):
    messages.append(lines[idx])
    idx += 1

print (rule_texts)
print (messages)

def get_rule_text(idx):
    text = rule_texts[idx]
    if text[0] == text[-1] == '"':
        print (text + "->" + text[1])
        return text[1]
    tkns = text.split(" ");
    result = ""
    for tkn in tkns:
        if tkn == '|':
            result += tkn
        else:
            result += "(" + get_rule_text(int(tkn)) + ")"
    print(text + "->" + result)
    return result

pattern = get_rule_text(0)
print (pattern)
cnt = 0
for m in messages:
    if re.fullmatch(pattern, m):
        print (m + " matches")
        cnt+=1
print (cnt)


