import re

text = '''42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba'''

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

messages = []
idx = 0
rule_texts = {}
l = lines[idx]
while l != "":
    (i, text) = tuple(l.split(":"))
    rule_texts[int(i)] = text.strip()
    idx += 1
    l = lines[idx]
idx+=1
while idx < len(lines):
    messages.append(lines[idx])
    idx += 1

print (rule_texts)
print (messages)

def get_rule_text(idx):
    text = rule_texts[idx]
    if text[0] == text[-1] == '"':
        #print (text + "->" + text[1])
        return text[1]
    tkns = text.split(" ");
    result = ""
    for tkn in tkns:
        if tkn == '|':
            result += tkn
        else:
            result += "(" + get_rule_text(int(tkn)) + ")"
    #print(text + "->" + result)
    return result

patterns = {}
for r in rule_texts:
    patterns[r] = get_rule_text(r).replace("(a)", "a").replace("(b)", "b")

patterns[0] = "(?P<g42>"+patterns[42]+")+(?P<g31>"+patterns[31]+")+"

print (str(42) + " " + patterns[42])
print (str(31) + " " + patterns[31])
cnt = 0
len42 = 0
len31 = 0
for m in messages:
    r = re.fullmatch(patterns[0], m)
    if r:
        nr42 = len(re.findall(patterns[42], m))
        len42 = len (r.group("g42"))
        nr31 = len(re.findall(patterns[31], m[nr42*len42:]))
        len31 = len (r.group("g31"))
print ("len  42: "+str(len42) + " / 31: " + str(len31))

for m in messages:
    r = re.fullmatch(patterns[0], m)
    if r:
        idx = 0
        nr42 = 0
        while idx < len(m)-len42:
            if re.fullmatch(patterns[42], m[idx:idx+len42]):
                nr42+=1
                idx+=len42
            else:
                break
        nr31 = int((len(m)/len42) - nr42)
        if (nr42>nr31):
            print (m + " matches " + str(nr42) + " / " + str(nr31) )
            cnt+=1

print (cnt)


