text = """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
"""

text = open("input.txt").read()

dx = [-1, -1, -1, 0, 0, 1, 1, 1]
dy = [-1, 0, 1, -1, 1, -1, 0, 1]

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

def count_neighbors (x,y,lines):
    maxy = len(lines)
    maxx = len(lines[0])
    cnt = 0
    for i in range(8):
        nx = x+dx[i]
        ny = y+dy[i]
        if (nx>=0) and (nx<maxx) and (ny>=0) and (ny<maxy):
            #print (x,y,nx,ny)
            if lines[ny][nx] == '#':
                cnt += 1
            #print(x, y, nx, ny, lines[y][x], cnt)
    return cnt


def calc_nextgen(lines):
    nextgen = []
    for y in range (len(lines)):
        nextgen.append("")
        for x in range (len(lines[y])):
            nr = count_neighbors(x,y,lines)
            #print (x,y,nr)
            if lines[y][x] == 'L' and nr == 0:
                nextgen[y] += '#'
            elif lines[y][x] == '#' and nr >= 4:
                nextgen[y] += 'L'
            else:
                nextgen[y] += lines[y][x]
    return nextgen

lastgen = []
while (lastgen != lines):
    lastgen = lines
    lines = calc_nextgen(lines)
    print (lines)

cnt = 0
for l in lines:
    for c in l:
        if c == '#':
            cnt+=1
print (cnt)