import re
from copy import deepcopy

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

def print_grid(grid):
    for l in grid:
        print (l)

def rotate_grid(grid):
    newgrid = []
    for x in range(len(grid[0])):
        gline = ''
        for y in range(len(grid)):
            gline += grid[y][-(x+1)]
        newgrid.append(gline)
    return newgrid


TOP = 0
RIGHT = 1
BOTTOM = 2
LEFT = 3
class Tile():
    def __init__(self, id, grid):
        self.id = id
        self.grid = grid
        self.create_borders()
        self.matchborder = ['', '', '', '']
        self.flipped = False

    def create_borders(self):
        self.borders=[self.grid[0], "", self.grid[-1][::-1], ""]
        for i in range (len(grid)):
            self.borders[1] += (grid[i][-1])
            self.borders[3] += (grid[-(i+1)][0])

    def flip(self):
        newgrid = self.grid[::-1]
        self.grid=newgrid
        newborders = [self.borders[2][::-1], self.borders[1][::-1], self.borders[0][::-1], self.borders[3][::-1]]
        self.borders = newborders
        self.flipped = True
        newborders = [self.matchborder[2], self.matchborder[1], self.matchborder[0], self.matchborder[3]]
        self.matchborder = newborders
        for i in range (4):
            if self.matchborder[i] != "":
                if self.matchborder[i][-2:] == "_F":
                    self.matchborder[i] = self.matchborder[i][:-2]
                else:
                    self.matchborder[i] += "_F"
        print ("flip " + self.id + " " + str(self.matchborder))

    def rotate_left(self):
        newborders = [self.borders[1], self.borders[2], self.borders[3], self.borders[0]]
        self.borders = newborders
        newborders = [self.matchborder[1], self.matchborder[2], self.matchborder[3], self.matchborder[0]]
        self.matchborder = newborders
        self.grid = rotate_grid(self.grid)

    def match_any_border(self, border_idx, other_tile):
        for i in range(4):
            if self.borders[border_idx] == other_tile.borders[i][::-1]:
                self.matchborder[border_idx] = other_tile.id #+ '_' + str(i)
            elif self.borders[border_idx] == other_tile.borders[i]:
                #self.matchborder[border_idx] = other_tile.id+ '_' + str(i)+"_F"
                self.matchborder[border_idx] = other_tile.id+ "_F"


tiles = {}
idx = 0
while idx < len(lines):
    print(lines[idx])
    m = re.search('Tile (\d+)', lines[idx])
    i = m.group(1)
    idx += 1
    grid = []
    while idx < len(lines) and lines[idx] != "":
        grid.append(lines[idx])
        idx += 1
    tiles[i] = Tile(i, grid)
    idx+=1

def match_borders():
    for t1 in tiles:
        for t2 in tiles:
            if t1 != t2:
                for i in range (4):
                    tiles[t1].match_any_border(i, tiles[t2])

# def find_flipped_tile():
#     for t in tiles:
#         for ot in tiles[t].matchborder:
#             if ot != "" and ot[-2:] == "_F":
#                 if tiles[ot[:-2]].flipped:
#                     tiles[t].flip()
#                     print ("flip "+tiles[t].id)
#                 else:
#                     tiles[ot[:-2]].flip()
#                     print ("flip "+ot[:-2])
#                 return True
#     return False

match_borders()
for t in tiles:
    print(t + " " + str(tiles[t].borders) + " " + str(tiles[t].matchborder))

# find top left corner
topleft = None
for t in tiles:
    if tiles[t].matchborder.count('')==2:
        topleft = tiles[t]
        break

#print (tiles[topleft].matchborder[TOP], tiles[topleft].matchborder[LEFT])
while (topleft.matchborder[TOP] != '') or (topleft.matchborder[LEFT] != ''):
    topleft.rotate_left()

def image_line(line, image):
    print ("image_line")
    while tiles[image[line][-1]].matchborder[RIGHT] != '':
        id = image[line][-1]
        nextid = tiles[image[line][-1]].matchborder[RIGHT]
        if (tiles[id].flipped):
            id += "_F"
        print (id+ " " + nextid)
        if nextid[-2:] == "_F":
            nextid = nextid[:-2]
            tiles[nextid].flip()
        while (tiles[nextid].matchborder[LEFT] != id):
            tiles[nextid].rotate_left()
        image[line].append(nextid)

image = []
image.append([topleft.id])
line = 0
image_line(0,image)
while tiles[image[line][0]].matchborder[BOTTOM] != '':
    print ("next line")
    id = image[line][0]
    nextid = tiles[image[line][0]].matchborder[BOTTOM]
    if (tiles[id].flipped):
        id += "_F"
    print(id + " " + nextid)
    if nextid[-2:] == "_F":
        nextid = nextid[:-2]
        tiles[nextid].flip()
    while (tiles[nextid].matchborder[TOP] != id):
        tiles[nextid].rotate_left()
    image.append([nextid])
    line += 1
    image_line(line,image)

#print (image)

image_grid = []
for image_line in image:
    for y in range(1, len(tiles[image_line[0]].grid)-1):
        line = ''
        for id in image_line:
            line += tiles[id].grid[y][1:-1]
        image_grid.append(line)

print_grid(image_grid)
print ()

monster = [
'                  # ',
'#    ##    ##    ###',
' #  #  #  #  #  #   ']

def is_monster(grid, monster, x, y):
    for ix in range (len(monster[0])):
        for iy in range (len(monster)):
            if monster[iy][ix] == '#' and grid[y+iy][x+ix] != '#':
                return False
    return True

def mark_monster(grid, monster, x, y):
    for ix in range (len(monster[0])):
        for iy in range (len(monster)):
            if monster[iy][ix] == '#':
                l = list(grid[y+iy])
                l[x+ix] = '0'
                grid[y+iy] = ''.join(l)

def find_monster(grid, monster):
    cnt = 0
    for x in range (len(grid[0]) - len(monster[0])):
        for y in range (len(grid)-len(monster)):
            if is_monster(grid, monster, x, y):
                cnt+=1
                mark_monster(grid, monster, x, y)
    if (cnt != 0):
        print_grid(grid)
    r = 0
    for l in grid:
        r += l.count('#')
    return cnt, r

print (find_monster(deepcopy(image_grid), monster))
#print_grid(image_grid)
image_grid = rotate_grid(image_grid)
print (find_monster(deepcopy(image_grid), monster))
image_grid = rotate_grid(image_grid)
print (find_monster(deepcopy(image_grid), monster))
image_grid = rotate_grid(image_grid)
print (find_monster(deepcopy(image_grid), monster))

image_grid = image_grid[::-1]

print (find_monster(deepcopy(image_grid), monster))
image_grid = rotate_grid(image_grid)
print (find_monster(deepcopy(image_grid), monster))
image_grid = rotate_grid(image_grid)
print (find_monster(deepcopy(image_grid), monster))
image_grid = rotate_grid(image_grid)
print (find_monster(deepcopy(image_grid), monster))
