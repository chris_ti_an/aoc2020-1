import re

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]


class Tile():
    def __init__(self, grid):
        self.grid = grid
        self.borders=[self.grid[0], "", self.grid[-1][::-1], ""]
        for i in range (len(grid)):
            self.borders[1] += (grid[i][-1])
            self.borders[3] += (grid[-(i+1)][0])
        self.matchborder = [False, False, False, False]

    def __str__(self):
        return str(self.borders)

    def match_any_border(self, border_idx, other_tile):
        for i in range(4):
            if self.borders[border_idx] == other_tile.borders[i][::-1] or self.borders[border_idx] == other_tile.borders[i]:
                return True
        return False


tiles = {}
idx = 0
while idx < len(lines):
    print(lines[idx])
    m = re.search('Tile (\d+)', lines[idx])
    i = m.group(1)
    idx += 1
    grid = []
    while idx < len(lines) and lines[idx] != "":
        grid.append(lines[idx])
        idx += 1
    tiles[i] = Tile(grid)
    idx+=1


for t1 in tiles:
    for t2 in tiles:
        if t1 != t2:
            for i in range (4):
                tiles[t1].matchborder[i] = tiles[t1].matchborder[i] or tiles[t1].match_any_border(i, tiles[t2])

result = 1
for t in tiles:
    if (tiles[t].matchborder.count(False) == 2):
        print (t + " "+ str(tiles[t].borders)+ " " + str(tiles[t].matchborder) + " corner")
        result *= int(t)
    else:
        #print (t + " "+ str(tiles[t].borders)+ " " + str(tiles[t].matchborder))
        pass

print (result)