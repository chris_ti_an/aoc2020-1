text ="""F10
N3
F7
R90
F11"""

text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

# east, north, west, south => L90 == dir+1 / R90 == dir-1

dx = [1, 0, -1, 0]
dy = [0, 1, 0, -1]
dir = 0
directions = ['east', 'north', 'west', 'south']
x = 0
y = 0

wx = 10
wy = 1

for l in lines:
    (code, nr) = (l[0], int(l[1:]))
    print (code, nr)
    if code=='N':
        wy+=nr
    elif code=='S':
        wy -= nr
    elif code=='E':
        wx+=nr
    elif code=='W':
        wx-=nr
    elif code == 'F':
        x += nr * wx
        y += nr * wy
    elif code == 'L':
        while (nr>0):
            wxn = -wy
            wyn = wx
            wx = wxn
            wy = wyn
            nr -= 90
    elif code=='R':
        while (nr>0):
            wxn = wy
            wyn = -wx
            wx = wxn
            wy = wyn
            nr -= 90
    print (dir, x, y, wx, wy)

print (abs(x)+abs(y))