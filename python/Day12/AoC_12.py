text ="""F10
N3
F7
R90
F11"""

text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

# east, north, west, south => L90 == dir+1 / R90 == dir-1

dx = [1, 0, -1, 0]
dy = [0, 1, 0, -1]
dir = 0
directions = ['east', 'north', 'west', 'south']
x = 0
y = 0

for l in lines:
    (code, nr) = (l[0], int(l[1:]))
    print (code, nr)
    if code=='N':
        y+=nr
    elif code=='S':
        y -= nr
    elif code=='E':
        x+=nr
    elif code=='W':
        x-=nr
    elif code == 'F':
        x += nr * dx[dir]
        y += nr * dy[dir]
    elif code == 'L':
        while (nr>0):
            dir = (dir + 1) % 4
            nr -= 90
    elif code=='R':
        while (nr>0):
            dir -= 1
            if dir<0:
                dir += 4
            nr -= 90
    print (dir, x, y)

print (abs(x)+abs(y))