import networkx
import networkx as nx
import re


text = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""

text = open("input.txt").read()


class bag:
    def __init__(self):
        self.can_contain = []
        self.can_be_in = []

bags = {}

#g = nx.Graph()
for l in text.split("\n"):
    if len(l) > 0:
        (bagname, rules) = tuple(l.split(" bags contain "))
        bags[bagname] = bag()

for l in text.split("\n"):
    if len(l) > 0:
        (bagname, rules) = tuple(l.split(" bags contain "))
        if rules != "no other bags.":
            print (bagname)
            for r in rules.split(","):
                m = re.search("(\d+) (.+) bag", r)
                print ("  - " + m.group(1) + " " + m.group(2))
                bags[bagname].can_contain.append((m.group(1), m.group(2)))
                bags[m.group(2)].can_be_in.append(bagname)
                #g.add_edge(bag, r)

print (bags)

def get_bags_can_be_in(list_of_bags, set_of_bags):
    print (list_of_bags)
    for l in list_of_bags:
        set_of_bags.add(l)
        get_bags_can_be_in(bags[l].can_be_in, set_of_bags)

set_of_bags = set()
get_bags_can_be_in(bags['shiny gold'].can_be_in, set_of_bags)
print (set_of_bags)
print(len(set_of_bags))

#networkx.drawing.draw(g)
#print (g)