import networkx
import networkx as nx
import re


text = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""

text = """shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."""

text = open("input.txt").read()


class bag:
    def __init__(self):
        self.can_contain = []
        self.can_be_in = []

bags = {}

#g = nx.Graph()
for l in text.split("\n"):
    if len(l) > 0:
        (bagname, rules) = tuple(l.split(" bags contain "))
        bags[bagname] = bag()

for l in text.split("\n"):
    if len(l) > 0:
        (bagname, rules) = tuple(l.split(" bags contain "))
        if rules != "no other bags.":
            #print (bagname)
            for r in rules.split(","):
                m = re.search("(\d+) (.+) bag", r)
                #print ("  - " + m.group(1) + " " + m.group(2))
                bags[bagname].can_contain.append((m.group(1), m.group(2)))
                bags[m.group(2)].can_be_in.append(bagname)
                #g.add_edge(bag, r)

#print (bags)

def count_bags_contained(list_of_bags, prefix):
    prefix += "    "
    count = 0
    if (len(list_of_bags)) == 0:
        return 0
    for (nr,name) in list_of_bags:
        print (prefix + nr + " " + name)
        count += int(nr) * (1 + count_bags_contained (bags[name].can_contain, prefix))
    return count

print (count_bags_contained(bags['shiny gold'].can_contain, ""))

#networkx.drawing.draw(g)
#print (g)