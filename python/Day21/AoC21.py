import re

text = """mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)"""

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

allergenset = {}
ingredients = set()
all_ingredients = []

for l in lines:
    m = re.search("(.*) \(contains (.*)\)", l)
    ingr = m.group(1).split(" ")
    all = m.group(2).split(", ")
    for a in all:
        if a not in allergenset:
            allergenset[a] = set(ingr)
        else:
            allergenset[a] = allergenset[a].intersection(set(ingr))
    for i in ingr:
        ingredients.add(i)
        all_ingredients.append(i)

print (allergenset)

with_allergens = {}
a2i = {}
changed = True
while changed:
    changed = False
    for a in allergenset:
        for i in with_allergens:
            allergenset[a].discard(i)
        if len(allergenset[a])==1:
            i = next(iter(allergenset[a]))
            with_allergens[i] = a
            a2i[a] = i
            changed = True
            allergenset[a] = set()

print (allergenset)
print (with_allergens)
print (ingredients)
print (all_ingredients)

cnt = 0
for i in ingredients:
    if i not in with_allergens:
        cnt += all_ingredients.count(i)

print (cnt)

result = []
for a in sorted(a2i.keys()):
    result.append(a2i[a])
print (','.join(result))