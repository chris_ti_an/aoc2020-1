text = """.#.
..#
###"""

text = """..#....#
##.#..##
.###....
#....#.#
#.######
##.#....
#.......
.#......"""

grid = {}
x1,x2,y1,y2,z1,z2 = 0,7,0,7,0,0

y = 0
for l in text.split("\n"):
    for x in range(len(l)):
        grid[(x,y,0)] = l[x]
    y+=1

def print_grid(x1,x2,y1,y2,z1,z2, grid):
    for z in range (z1,z2+1):
        print ("z="+str(z))
        for y in range (y1,y2+1):
            for x in range(x1, x2 + 1):
                if (x,y,z) in grid:
                    print (grid[(x,y,z)],end="")
                else:
                    print (".",end="")
            print()

dirs = []
for x in range (-1,2):
    for y in range(-1, 2):
        for z in range(-1, 2):
            if (x!=0) or (y!=0) or (z!=0):
                dirs.append((x,y,z))

print_grid (0,2,0,2,0,0,grid)

def count_next(x,y,z, grid):
    cnt = 0
    for (dx,dy,dz) in dirs:
        np = (x+dx, y+dy, z+dz)
        if np in grid and grid[np] == '#':
            cnt += 1
    return cnt


def next_gen (x1,x2,y1,y2,z1,z2, grid):
    new_grid = {}
    active = 0
    for z in range(z1, z2 + 1):
        for y in range(y1, y2 + 1):
            for x in range(x1, x2 + 1):
                if (x,y,z) not in grid:
                    grid[(x,y,z)] = "."
                cnt = count_next(x,y,z,grid)
                if cnt == 3 and grid[(x, y, z)] == ".":
                    new_grid[(x, y, z)] = "#"
                    active+=1
                elif (cnt == 3 or cnt == 2) and grid[(x, y, z)] == "#":
                    new_grid[(x, y, z)] = "#"
                    active+=1
                else:
                    new_grid[(x, y, z)] = "."
    return active, new_grid

cnt = 0
for c in range(6):
    print ("=============")
    x1-=1
    x2+=1
    y1-=1
    y2+=1
    z1-=1
    z2+=1
    cnt, new_grid = next_gen(x1,x2,y1,y2,z1,z2, grid)
    #print_grid(x1,x2,y1,y2,z1,z2, new_grid)
    grid = new_grid

print (cnt)






