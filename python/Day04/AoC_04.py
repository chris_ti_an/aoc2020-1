# -*- coding: utf-8 -*-

import copy
text = """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"""

text = open("input.txt").read()



lines = text.split('\n')
passports = []
passport = {}
for l in lines:
    if l=='':
        #print (passport)
        passports.append(passport)
        passport = {}
    else:
        fields = l.split()
        for f in fields:
            items = f.split(':')
            passport[items[0]] = items[1]
passports.append(passport)
nrValid = 0
for p in passports:
    if 'byr' in p.keys() and 'iyr' in p.keys() and 'eyr' in p.keys() and 'hgt' in p.keys() and 'hcl' in p.keys() and 'ecl' in p.keys() and 'pid' in p.keys():
        nrValid += 1

print(nrValid)


