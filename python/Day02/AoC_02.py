import re

text = """1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc"""

text = open("input.txt").read()

lines=text.split('\n')

def countChars(lines):
    numpwd = 0
    for l in lines:
        if (len(l) >0):
            #print (l)
            m = re.search("(\d+)-(\d+) (.): (.+)", l)
            min = int(m.group(1))
            max = int(m.group(2))
            char = m.group(3)
            password = m.group(4)
            count = 0
            for c in password:
                if c == char:
                    count += 1
            if (count>=min) and (count <=max):
                #print (min, max, char, password, count)
                numpwd += 1
    return numpwd

def char_position(lines):
    numpwd = 0
    for l in lines:
        if (len(l) >0):
            #print (l)
            m = re.search("(\d+)-(\d+) (.): (.+)", l)
            pos1 = int(m.group(1))-1
            pos2 = int(m.group(2))-1
            char = m.group(3)
            password = m.group(4)
            count = 0
            if (password[pos1]==char):
                count += 1
            if (password[pos2]==char):
                count +=1
            if (count==1):
                print (pos1, pos2, char, password, password[pos1], password[pos2])
                numpwd += 1
    return numpwd

print (countChars(lines))
print (char_position(lines))
