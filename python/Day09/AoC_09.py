text = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576
"""
text = open("input.txt").read()

lines = text.split('\n')

numbers = [int(l) for l in lines if l != ""]

def is_sum_in_array(n, sum, l):
    for i in range (len(n)-1):
        for j in range (1,len(n)):
            if (n[i]+n[j] == sum):
                return True
    return False

length = 25
index = 25
for idx in range (25,len(numbers)):
    if not is_sum_in_array(numbers[idx-25:idx], numbers[idx], length):
        print (numbers[idx])
