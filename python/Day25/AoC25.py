def transform(subject, loops):
    value = 1
    for l in range (loops):
        value = (value*subject) % 20201227
    return value

for l in range (15):
    print (transform(7,l))

def get_loop(key,subject):
    loops = 0
    value = 1
    while value != key:
        value = (value * subject) % 20201227
        loops+=1
    return loops

#1965712
#19072108
print (get_loop(17807724, 7))
print (get_loop(5764801, 7))
doorloops = get_loop(1965712, 7)
print (transform(19072108, doorloops))
