numbers = [0,3,6]
numbers = [9,19,1,6,0,5,4]

turns = {}
t = 1
for n in numbers:
    turns[n] = [t]
    t+=1

for i in range (30000000-len(numbers)):
    turn = len(numbers)+1
#    print ("========== turn "+str(turn))
    last = numbers[-1]
    nr = 0
    if last in turns:
        if len(turns[last])>1:
            nr = turns[last][-1]-turns[last][-2]
    numbers.append(nr)
    if not nr in turns:
        turns[nr] = [turn]
    else:
        turns[nr].append(turn)
#    print (turn, nr)

print (numbers[-1])