# https://rosettacode.org/wiki/Chinese_remainder_theorem#Python_3.6

from functools import reduce


def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a * b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1


text="""939
7,13,x,x,59,x,31,19"""

text = open("input.txt").read()

lines = text.split('\n')

time = int(lines[0])

a = []
n = []
d = 0
for id in lines[1].split(','):
    if id != 'x':
        i = int (id)
        a.append(i-d)
        n.append(int(id))
    d+=1

print (a, n)
result = chinese_remainder(n,a)
print (result)
result = 1068781
for x in n:
    print (result % x)

