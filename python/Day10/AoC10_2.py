import networkx as nx

text = """16
10
15
5
1
11
7
19
6
12
4"""

text="""28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3"""

text = open("input.txt").read()

lines = text.split('\n')
numbers = [int(l) for l in lines if l != ""]
numbers.append(0)
numbers.append(max(numbers)+3)
numbers.sort()

cnt = []
cnt.append(1)

for i in range (1,len(numbers)):
    c = 0
    for idx in range (1,4):
        diff = numbers[i] - numbers[i-idx]
        if (i-idx)>=0 and diff<=3:
            c += cnt[i-idx]
    cnt.append(c)

print (cnt)

