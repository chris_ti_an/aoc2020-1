from copy import deepcopy

text = """Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10"""

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

deck1 = []
deck2 = []
idx = 1
while lines[idx] != "":
    deck1.append(int(lines[idx]))
    idx += 1

idx+=2
while idx < len(lines) and lines[idx] != "":
    deck2.append(int(lines[idx]))
    idx += 1

print (deck1)
print (deck2)

round = 1
def play(round, game, d1, d2):
    config = set()
    while len(d1)>0 and len(d2)>0:
        print ("Round "+str(round)+" Game "+str(game)+" "+str(d1)+str(d2))
        c1 = d1.pop(0)
        c2 = d2.pop(0)
        if c1<=len(d1) and c2<=len(d2):
            d1c = deepcopy(d1[:c1])
            d2c = deepcopy(d2[:c2])
            (winner,d) = play(1, game+1, d1c, d2c)
        else:
            if c1 > c2:
                winner = 1
            else:
                winner = 2

        if winner == 1:
            d1.append(c1)
            d1.append(c2)
        else:
            d2.append(c2)
            d2.append(c1)
        round += 1
        if (tuple(d1),tuple(d2)) in config:
            print ("prevent loop")
            return (1,d1)
        config.add((tuple(d1),tuple(d2)))
    if len(d1) > 0:
        return (1,d1)
    else:
        return (2,d2)

(p,win) = play(1, 1, deck1, deck2)

score = 0
for i in range (1, len(win)+1):
    score += i*win[-i]
print (score)