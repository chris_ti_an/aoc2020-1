text = """Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10"""

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

deck1 = []
deck2 = []
idx = 1
while lines[idx] != "":
    deck1.append(int(lines[idx]))
    idx += 1

idx+=2
while idx < len(lines) and lines[idx] != "":
    deck2.append(int(lines[idx]))
    idx += 1

print (deck1)
print (deck2)

round = 1
while len(deck1)>0 and len(deck2)>0:
    c1 = deck1.pop(0)
    c2 = deck2.pop(0)
    if c1 > c2:
        deck1.append(c1)
        deck1.append(c2)
    else:
        deck2.append(c2)
        deck2.append(c1)
    print ("Round "+str(round)+str(deck1)+str(deck2))
    round += 1

if len(deck1) > 0:
    win = deck1
else:
    win = deck2

score = 0
for i in range (1, len(win)+1):
    score += i*win[-i]
print (score)