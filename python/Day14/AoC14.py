text = """mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0"""

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

memory = {}
mask = "X"*36

def init_memory(address):
    if not address in memory:
        memory[address] = "0"*36

def get_code_value(line):
    tkns = line.split("=")
    return (tkns[0].strip(), tkns[1].strip())

for l in lines:
    (code, value) = get_code_value(l)
    #print (l, code, value)
    if code=='mask':
        mask = value
        print ("mask = ", mask)
    else:
        address = int(code[4:-1])
        init_memory(address)
        bits = '{0:036b}'.format(int(value))
        result = ""
        for i in range(36):
            if mask[i] == 'X':
                result += bits[i]
            else:
                result += mask[i]
        memory[address] = result
        print (address, memory[address], int(memory[address], 2))

sum = sum([int(x,2) for x in memory.values()])
print (sum)
