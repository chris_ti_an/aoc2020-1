text = """mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1"""

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

memory = {}
mask = "X"*36

def init_memory(address):
    if not address in memory:
        memory[address] = 0

def get_code_value(line):
    tkns = line.split("=")
    return (tkns[0].strip(), tkns[1].strip())

def replace_bits(address, nr, nrx):
    mask = bin(nr)[2:]
    mask = '0'*(nrx-len(mask)) + mask
    idx = 0
    result = ""
    for c in address:
        if c =='X':
            result += mask[idx]
            idx+=1
        else:
            result += c
    return result

for l in lines:
    (code, value) = get_code_value(l)
    print (l, code, value)
    if code=='mask':
        mask = value
        print ("mask = ", mask)
    else:
        address = int(code[4:-1])
        init_memory(address)
        bits = '{0:036b}'.format(int(address))
        result = ""
        nrx = 0
        for i in range(36):
            if mask[i] == '0':
                result += bits[i]
            elif mask[i] == '1':
                    result += '1'
            else:
                result += 'X'
                nrx += 1
        #print ("    " + result)
        for i in range (pow(2,nrx)):
            address = replace_bits(result, i, nrx)
            memory[address] = int(value)
            #print ("    " + address + " " + str(value))


sum = sum(memory.values())
print (sum)
