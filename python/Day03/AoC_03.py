# -*- coding: utf-8 -*-

text = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"""

text = open("input.txt").read()



lines = text.split()
xmax = len(lines[0])
def traverse (xstep, ystep):
    x = 0
    y = 0
    trees = 0
    while y < len(lines):
        if lines[y][x] == '#':
            trees +=1
        x = (x+xstep) % xmax
        y += ystep
    return trees

result = traverse(1,1) * traverse(3,1) * traverse(5,1) *traverse(7,1) * traverse(1,2)
print (result)

