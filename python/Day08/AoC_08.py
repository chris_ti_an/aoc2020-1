text = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""

text = open("input.txt").read()


code = text.split ("\n")

acc = 0
ip_visited = set()

def execute_next_line(ip, acc):
    (cmd, value) = tuple(code[ip].split())
    if (cmd == "acc"):
        acc += int(value)
    elif (cmd == "jmp"):
        return ip + int(value), acc
    return ip+1, acc

def run_program():
    acc = 0
    ip = 0
    while not ip in ip_visited:
        ip_visited.add(ip)
        (ip, acc) = execute_next_line(ip, acc)
        if (ip == len(code)):
            return (True, acc)
    return (False, acc)

(result, acc) = run_program()
print(result, acc)
