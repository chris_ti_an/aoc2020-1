# -*- coding: utf-8 -*-


import re

text = """abc

a
b
c

ab
ac

a
a
a
a

b"""

text = open("input.txt").read()

lines = text.split("\n")

groups = []
chars = set(list("abcdefghijklmnopqrstuvwxyz"))
for l in lines:
    if len(l)==0:
        groups.append(chars)
        print ("result ", sorted(chars))
        chars = set(list("abcdefghijklmnopqrstuvwxyz"))
    else:
        selection = set(list(l))
        print ("{0} + {1} = {2}".format(sorted(chars), sorted(selection), sorted(chars.intersection(selection))))
        chars = chars.intersection(selection)

sum = 0
for g in groups:
    print (len(g) , sorted(g))
    sum += len(g)

print (sum)

