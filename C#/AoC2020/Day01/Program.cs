﻿using System;
using System.Collections.Generic;

namespace AoC2020.Day01
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<string> input = new List<string> { "1721", "979", "366", "299", "675", "1456" };
            List<String> input = AoC2020.FileReader.ReadFile("input.txt");

            int nr = 0;
            for (int i = 0; i < input.Count - 2; ++i)
            {
                for (int j = i + 1; j < input.Count - 1; ++j)
                {
                    for (int k = j + 1; k < input.Count; ++k)
                    {
                        if (2020 == Int32.Parse(input[i]) + Int32.Parse(input[j]) + Int32.Parse(input[k]))
                        {
                            Console.WriteLine($"{i} - {j} - {k}: {Int32.Parse(input[i]) * Int32.Parse(input[j]) * Int32.Parse(input[k])}");
                        }
                        nr++;
                    }
                }
            }
            Console.WriteLine($"{input.Count} elements - {nr} combinations");
        }
    }
}
